

## Авторизация на api.vscale.io
Варианты:  

1) Хедер `X-Token` значение переменной окружения `VSCALE_TOKEN`  
2) Cookies `uid`,`sid` соответственно значения переменных окружения `VSCALE_UID`, `VSCALE_SID`, используются когда не казана переменная `VSCALE_TOKEN` 

## Запуск и swagger

#### Запуск:
`docker-compose up -d`  

#### Swagger:
[Swagger(localhost)](htts://localhost:8080/api/docs)

при `API_PREFIX=/api` url для сваггера http://хост_с_докером:8080/api/docs
