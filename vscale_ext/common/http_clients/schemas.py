from datetime import datetime
from typing import List, Optional, Dict, Union, Type

from pydantic import BaseModel

from vscale_ext.common.http_clients.enums import RPlan, Templates, Locations


class CreateServerRequestBody(BaseModel):
    name: str
    location: Locations
    make_from: Templates
    rplan: RPlan
    do_start: Optional[bool]
    keys: List[int]
    password: str


class BaseServer(BaseModel):
    ctid: int
    name: str
    status: str
    location: Locations
    rplan: RPlan
    keys: List[int]
    tags: List[str]
    public_address: Dict
    private_address: Dict
    made_from: Templates
    hostname: str
    created: str
    active: bool
    locked: bool
    deleted: Optional[Union[Dict, str, bool]]
    block_reason: Optional[Union[Dict, str, bool]]
    block_reason_custom: Optional[Union[Dict, str, bool]]
    date_block: Optional[str]


class CreatedServer(BaseServer):
    pass


class DeletedServer(BaseServer):
    pass
