import json
import random
import string
from abc import ABC, abstractmethod
from typing import Dict, Any, Type, Optional

from aiohttp import ClientSession, ClientTimeout, ClientResponse

from vscale_ext.common.http_clients.enums import Locations, Templates, RPlan
from vscale_ext.common.http_clients.schemas import CreateServerRequestBody, CreatedServer
from vscale_ext.core.settings import settings


class BaseRequest(ABC):
    METHOD: str
    API_ENDPOINT_TEMPLATE: str

    def __init__(self, api_url: str, query_parameters: Dict[str, str], headers: Optional[Dict[str, str]], timeout: int,
                 cookies: Optional[Dict[str, str]]):
        self._api_url = api_url
        self._query_parameters = query_parameters
        self._headers = headers
        self._timeout = timeout
        self._cookies = cookies

    @abstractmethod
    def make_request_json_body(self) -> Optional[Dict[str, Any]]:
        ...

    def as_dict(self) -> Dict[str, Any]:
        return dict(
            url=self.build_url(),
            method=self.METHOD,
            headers=self._headers,
            timeout=ClientTimeout(total=self._timeout),
            json=self.make_request_json_body(),
            cookies=self._cookies,
        )

    def build_url(self):
        if self._query_parameters:
            enpoint = self.API_ENDPOINT_TEMPLATE.format(**self._query_parameters)
        else:
            enpoint = self.API_ENDPOINT_TEMPLATE
        return self._api_url + enpoint


class CreateServerRequest(BaseRequest):
    METHOD: str = 'POST'
    API_ENDPOINT_TEMPLATE: str = '/scalets'
    DEFAULT_LOCATION: Locations = Locations.SPB
    DEFAULT_CONFIGURATION_TEMPLATE: Templates = Templates.UBUNTU_18
    DEFAULT_RPLAN: RPlan = RPlan.SMALL
    DEFAULT_SSH_KEYS = []

    def make_request_json_body(self):
        name = ''.join((random.choice(string.ascii_lowercase) for _ in range(32)))
        ssh_keys = self.DEFAULT_SSH_KEYS
        password = ''.join((random.choice(string.ascii_lowercase) for _ in range(8))) if not ssh_keys else None
        request_body = CreateServerRequestBody(  # TODO: add parameters from settings or autogenerate
            name=name,
            location=self.DEFAULT_LOCATION,
            make_from=self.DEFAULT_CONFIGURATION_TEMPLATE,
            rplan=self.DEFAULT_RPLAN,
            keys=self.DEFAULT_SSH_KEYS,
            password=password,
        )
        return json.loads(request_body.json())


class DeleteServerRequest(BaseRequest):
    METHOD = 'DELETE'
    API_ENDPOINT_TEMPLATE = '/scalets/{ctid}'

    def make_request_json_body(self) -> None:
        return None


class VscaleHttpClient:
    TOKEN_HEADER_NAME: str = 'X-Token'

    def __init__(self, session: ClientSession = None):
        self._session: ClientSession = session or ClientSession()
        self._api_url: str = f'https://{settings.VSCALE_API_HOST}{settings.VSCALE_API_PREFIX}'
        self._token: str = settings.VSCALE_TOKEN
        self._cookies: Dict = dict(uid=settings.VSCALE_UID, sid=settings.VSCALE_SID)

    async def _do_request(self, request_class: Type[BaseRequest], query_parameters: Dict[str, str] = None, timeout=None) -> ClientResponse:
        if timeout is None:
            timeout = settings.VSCALE_COMMON_TIMEOUT
        headers = None
        cookies = None
        if self._token:
            headers = {self.TOKEN_HEADER_NAME: self._token}
        else:
            cookies = self._cookies
        request_parameters = request_class(self._api_url, query_parameters, headers, timeout, cookies)

        return await self._session.request(**request_parameters.as_dict())

    def close_session(self) -> None:
        self._session.close()

    async def create_server(self) -> ClientResponse:
        return await self._do_request(CreateServerRequest)

    async def delete_server(self, ctid: int) -> ClientResponse:
        query_parameters = {'ctid': str(ctid)}
        return await self._do_request(DeleteServerRequest, query_parameters)
