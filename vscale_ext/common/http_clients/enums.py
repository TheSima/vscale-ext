from enum import Enum


class RPlan(Enum):
    SMALL = 'small'
    MEDIUM = 'medium'
    LARGE = 'large'
    HUGE = 'huge'
    MONSTER = 'monster'


class Templates(Enum):
    UBUNTU_14 = 'ubuntu_14.04_64_001_preseed'
    UBUNTU_16 = 'ubuntu_16.04_64_001_master'
    UBUNTU_18 = 'ubuntu_18.04_64_001_master'
    UBUNTU_20 = 'ubuntu_20.04_64_001_master'
    CENTOS_7 = 'centos_7_64_001_master'
    CENTOS_8 = 'CentOS_8_64_001_master'
    DEBIAN_9 = 'debian_9_64_001_master'
    DEBIAN_10 = 'debian_10_64_001_master'
    FEDORA_31 = 'Fedora_31_64_001_master'
    FEDORA_32 = 'Fedora_32_64_001_master'

    AJENTI = 'ubuntu_20.04_64_001_ajenti'
    WORDPRESS = 'ubuntu_20.04_64_001_wordpress'
    DOCKER = 'ubuntu_20.04_64_001_docker'
    GOGS = 'ubuntu_20.04_64_001_gogs'
    GITLAB = 'ubuntu_20.04_64_001_gitlab'
    NODEJS = 'ubuntu_20.04_64_001_nodejs'
    MONGODB = 'ubuntu_20.04_64_001_mongodb'
    BITRIX = 'ubuntu_20.04_64_001_bitrix'
    LAMP = 'ubuntu_20.04_64_001_lamp'
    DJANGO = 'ubuntu_20.04_64_001_django'
    REDMINE = 'ubuntu_20.04_64_001_redmine'
    REDIS = 'ubuntu_20.04_64_001_redis'
    JENKINS = 'ubuntu_20.04_64_001_jenkins'

    DEBIAN_FASTPANEL = 'debian_10_64_001_fastpanel'


class Locations(Enum):
    SPB = 'spb0'
    MSK = 'msk0'
