from starlette import status

from vscale_ext.core.errors import BaseHTTPException


class CreateServerError(BaseHTTPException):
    default_status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail: str = 'Internal error during servers creation'


class DeleteServerError(BaseHTTPException):
    default_status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail: str = 'Internal error during servers deletion'
