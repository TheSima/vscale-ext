import asyncio
from dataclasses import dataclass
from functools import partial
from typing import Tuple, Coroutine, Any, List, Iterable, Callable, Union, Optional

from aiohttp import ClientResponse
from pydantic import parse_obj_as
from starlette import status

from vscale_ext.common.http_clients.schemas import CreatedServer, DeletedServer
from vscale_ext.common.http_clients.vscale import VscaleHttpClient
from vscale_ext.core.settings import settings
from vscale_ext.repositories.redis.servers import ServerRepository
from vscale_ext.services.servers.errors import CreateServerError, DeleteServerError


@dataclass
class ServerService:
    vscale_client: VscaleHttpClient
    server_repository: ServerRepository

    async def _run_requests_and_retry(
            self,
            coro_func__run_after: Iterable[Tuple[Callable[[], Coroutine], int]],
            ok_status_codes: Union[List[int], int],
            on_fail_raise: Callable,
            on_fail_do: Optional[Callable] = None,
    ) -> List[ClientResponse]:
        if isinstance(ok_status_codes, int):
            ok_status_codes = (ok_status_codes,)

        finished_tasks = []
        failed_tasks = []
        exceptions = []
        tasks = [self._sleep_and_do(coro_func(), run_after) for coro_func, run_after in coro_func__run_after]
        retries = 0
        while tasks:
            results = await asyncio.gather(*tasks, return_exceptions=True)
            new_coro_func__run_after = []
            to_retry = []
            task_id = 0
            for r in results:
                if isinstance(r, Exception):
                    exceptions.append(r)
                elif r.status == status.HTTP_429_TOO_MANY_REQUESTS:
                    to_retry.append(r)
                    retry_after = self._parse_retry_after(r.headers.get('Retry-After'))
                    new_coro_func__run_after.append((coro_func__run_after[task_id][0], retry_after))
                elif r.status in ok_status_codes:
                    finished_tasks.append(r)
                else:
                    failed_tasks.append(r)
                task_id += 1

            if failed_tasks or exceptions:
                break

            retries += 1
            if retries > settings.VSCALE_API_MAX_RETRIES:
                failed_tasks.extend(to_retry)
                break

            coro_func__run_after = new_coro_func__run_after
            tasks = [self._sleep_and_do(coro_func(), run_after) for coro_func, run_after in coro_func__run_after]

        if failed_tasks or exceptions:
            if on_fail_do:
                await on_fail_do(finished_tasks, failed_tasks, exceptions)
            details = self._prepare_details(failed_tasks, exceptions)
            raise on_fail_raise(detail=str(details))  # XXX: Detail information available for client

        return finished_tasks

    async def _sleep_and_do(self, coro: Coroutine, after: int) -> Any:
        if after:
            await asyncio.sleep(after)
        return await coro

    def _parse_retry_after(self, retry_after: str, default: int = 0) -> int:
        if retry_after is None:
            return default
        try:
            return int(retry_after)
        except ValueError:
            # TODO: parse <http-date> format Retry-After: Wed, 21 Oct 2015 07:28:00 GMT
            return default

    def _prepare_details(self, failed, exceptions) -> List[Union[ClientResponse, Exception]]:
        details = [f.headers.get('Vscale-Error-Message', f) for f in failed]
        details.extend(exceptions)
        return details

    async def create_servers(self, quantity: int) -> List[CreatedServer]:
        coro_func__run_after = [(self.vscale_client.create_server, 0) for _ in range(quantity)]

        responses = await self._run_requests_and_retry(
            coro_func__run_after,
            ok_status_codes=status.HTTP_201_CREATED,
            on_fail_raise=CreateServerError,
            on_fail_do=self._on_fail_create_servers,
        )
        created_servers = []
        for r in responses:
            created_server = parse_obj_as(CreatedServer, (await r.json()))
            await self.server_repository.create_server(created_server.ctid)
            created_servers.append(created_server)

        return created_servers

    async def _on_fail_create_servers(
            self,
            finished: List[ClientResponse],
            _: List[ClientResponse],
            __: List[Exception]
    ):
        created_servers_ctids = [(await f.json())['ctid'] for f in finished]
        await self.delete_servers(created_servers_ctids)

    async def delete_servers(self, ctids: List[int]) -> List[DeletedServer]:
        coro_func__run_after = ((partial(self.vscale_client.delete_server, ctid), 0) for ctid in ctids)

        responses = await self._run_requests_and_retry(
            coro_func__run_after,
            ok_status_codes=status.HTTP_200_OK,
            on_fail_raise=DeleteServerError,
        )
        deleted_servers = []
        for r in responses:
            deleted_server = parse_obj_as(DeletedServer, (await r.json()))
            await self.server_repository.delete_server(deleted_server.ctid)
            deleted_servers.append(deleted_server)

        return deleted_servers

    async def delete_all_servers(self):
        ctids = await self.server_repository.get_all_ctids()
        return await self.delete_servers(ctids)
