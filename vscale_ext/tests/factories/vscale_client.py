import string
import factory
from factory import fuzzy

from vscale_ext.common.http_clients.schemas import CreatedServer, DeletedServer
from vscale_ext.common.http_clients.enums import Locations, RPlan, Templates

class CreatedServerFactory(factory.Factory):
    class Meta:
        model = CreatedServer

    ctid = factory.Faker('pyint', min_value=1)
    name = factory.Faker('uuid4')
    status = fuzzy.FuzzyText(length=8, chars=string.ascii_letters, prefix='')
    location = fuzzy.FuzzyChoice(Locations)
    rplan = fuzzy.FuzzyChoice(RPlan)
    keys = []
    tags = []
    public_address = {}
    private_address = {}
    made_from = fuzzy.FuzzyChoice(Templates)
    hostname = ''
    created = ''
    active = fuzzy.FuzzyChoice([True, False])
    locked = fuzzy.FuzzyChoice([True, False])
    deleted = None
    block_reason = None
    block_reason_custom = None
    date_block = None

class DeletedServerFactory(factory.Factory):
    class Meta:
        model = DeletedServer

    ctid = factory.Faker('pyint', min_value=1)
    name = factory.Faker('uuid4')
    status = fuzzy.FuzzyText(length=8, chars=string.ascii_letters, prefix='')
    location = fuzzy.FuzzyChoice(Locations)
    rplan = fuzzy.FuzzyChoice(RPlan)
    keys = []
    tags = []
    public_address = {}
    private_address = {}
    made_from = fuzzy.FuzzyChoice(Templates)
    hostname = factory.Faker('uuid4')
    created = ''
    active = fuzzy.FuzzyChoice([True, False])
    locked = fuzzy.FuzzyChoice([True, False])
    deleted = None
    block_reason = None
    block_reason_custom = None
    date_block = None