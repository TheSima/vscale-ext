from fastapi.testclient import TestClient

from vscale_ext.main import app
from .fixture import *


@pytest.fixture
def application():
    return app


@pytest.fixture
def client(application):
    return TestClient(application)
