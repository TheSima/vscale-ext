import pytest
from aioredis import Redis

from vscale_ext.repositories.redis.servers import ServerRepository
from vscale_ext.repositories.errors import UniqueViolationError, NotFoundError


class TestServerRepository:

    @pytest.mark.asyncio
    async def test_create_server__success(self, server_repository: ServerRepository, redis: Redis):
        ctid: int = 1111

        await server_repository.create_server(ctid)

        members = await redis.smembers(server_repository._servers_set_key)
        members = list(map(int, members))
        assert members == [ctid]

    @pytest.mark.asyncio
    async def test_create_server__failed(self, server_repository: ServerRepository):
        ctid: int = 1111

        await server_repository.create_server(ctid)

        with pytest.raises(UniqueViolationError):
            await server_repository.create_server(ctid)

    @pytest.mark.asyncio
    async def test_delete_server__success(self, server_repository: ServerRepository, redis: Redis):
        ctid: int = 1111
        await server_repository.create_server(ctid)
        await server_repository.create_server(ctid + 1)

        await server_repository.delete_server(ctid + 1)

        members = await redis.smembers(server_repository._servers_set_key)
        members = list(map(int, members))
        assert members == [ctid]

    @pytest.mark.asyncio
    async def test_delete_server__failed(self, server_repository: ServerRepository):
        ctid: int = 1111

        with pytest.raises(NotFoundError):
            await server_repository.delete_server(ctid)
