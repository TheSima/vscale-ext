import pytest

from vscale_ext.repositories.redis.connection import get_redis_pool
from vscale_ext.repositories.redis.servers import ServerRepository


@pytest.fixture
async def redis():
    redis = await get_redis_pool()
    redis.flushdb()
    yield redis
    redis.close()
    await redis.wait_closed()


@pytest.fixture
def server_repository(redis):
    return ServerRepository(redis=redis)
