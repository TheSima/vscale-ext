import dataclasses
from dataclasses import dataclass
from typing import Dict

import pytest
from aiohttp import ClientResponse

from vscale_ext.common.http_clients.vscale import VscaleHttpClient


@pytest.fixture
def vscale_client():
    return VscaleHttpClient()


@pytest.fixture
def fake_vscale_client():
    return FakeVscaleHttpClient()


class FakeVscaleHttpClient:
    async def create_server(self) -> ClientResponse:
        return FakeClientResponse()

    async def delete_server(self, ctid: int) -> ClientResponse:
        return FakeClientResponse()


@pytest.fixture
def get_fake_response():
    def __wrap__(**kwargs):
        return FakeClientResponse(**kwargs)
    return __wrap__


@dataclass
class FakeClientResponse:
    status: int
    json_return: Dict = dataclasses.field(default_factory=dict)
    headers: Dict = dataclasses.field(default_factory=dict)

    async def json(self):
        return self.json_return
