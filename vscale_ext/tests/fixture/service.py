import pytest

from vscale_ext.services.servers import ServerService


@pytest.fixture
def server_service(server_repository, vscale_client) -> ServerService:
    return ServerService(vscale_client=vscale_client, server_repository=server_repository)


@pytest.fixture
def server_service_with_mocked_vscale_client(server_repository, fake_vscale_client) -> ServerService:
    return ServerService(vscale_client=fake_vscale_client, server_repository=server_repository)
