import pytest
import starlette.status

from vscale_ext.services.servers import ServerService
from vscale_ext.tests.factories.vscale_client import CreatedServerFactory, DeletedServerFactory
from vscale_ext.services.servers.errors import CreateServerError, DeleteServerError

VSCALE_HTTP_CLIENT = 'vscale_ext.common.http_clients.vscale.VscaleHttpClient'


class TestServerService:
    # TODO: mock redis repository

    @pytest.mark.asyncio
    async def test_create_servers__success(self, server_service: ServerService, get_fake_response, mocker, redis):
        ok_create_response_body = CreatedServerFactory()
        ok_create_fake_response = get_fake_response(status=starlette.status.HTTP_201_CREATED,
                                                    json_return=ok_create_response_body)
        mocker.patch(f'{VSCALE_HTTP_CLIENT}.create_server', return_value=ok_create_fake_response)
        quantity = 1

        await server_service.create_servers(quantity)

        members = await redis.smembers(server_service.server_repository._servers_set_key)
        members = list(map(int, members))
        assert members == [ok_create_response_body.ctid]

    @pytest.mark.asyncio
    async def test_create_servers__failed(self, server_service: ServerService, get_fake_response, mocker):
        fail_create_fake_response = get_fake_response(status=starlette.status.HTTP_409_CONFLICT)
        mocker.patch(f'{VSCALE_HTTP_CLIENT}.create_server', return_value=fail_create_fake_response)
        quantity = 1

        with pytest.raises(CreateServerError):
            await server_service.create_servers(quantity)

    @pytest.mark.asyncio
    async def test_delete_servers__success(self, server_service: ServerService, get_fake_response, mocker, redis):
        ok_delete_response_body = DeletedServerFactory()
        ok_delete_fake_response = get_fake_response(status=starlette.status.HTTP_200_OK,
                                                    json_return=ok_delete_response_body)
        mocker.patch(f'{VSCALE_HTTP_CLIENT}.delete_server', return_value=ok_delete_fake_response)
        await server_service.server_repository.create_server(ok_delete_response_body.ctid)

        await server_service.delete_servers([ok_delete_response_body.ctid])

        members = await redis.smembers(server_service.server_repository._servers_set_key)
        members = list(map(int, members))
        assert members == []

    @pytest.mark.asyncio
    async def test_delete_servers__failed(self, server_service: ServerService, get_fake_response, mocker):
        fail_delete_fake_response = get_fake_response(status=starlette.status.HTTP_409_CONFLICT)
        mocker.patch(f'{VSCALE_HTTP_CLIENT}.delete_server', return_value=fail_delete_fake_response)
        ctid: int = 1111

        with pytest.raises(DeleteServerError):
            await server_service.delete_servers([ctid])

    @pytest.mark.asyncio
    async def test_delete_all_servers__success(self, server_service: ServerService, get_fake_response, mocker, redis):
        servers_ctids = [111, 222, 333]
        def generate_fake_responses():
            for ctid in servers_ctids:
                ok_delete_response_body = DeletedServerFactory(ctid=ctid)
                ok_delete_fake_response = get_fake_response(status=starlette.status.HTTP_200_OK,
                                                            json_return=ok_delete_response_body)
                yield ok_delete_fake_response
        generator = generate_fake_responses()
        async def fake_delete_server(*args, **kwargs):
            n = next(generator)
            return n

        mocker.patch(f'{VSCALE_HTTP_CLIENT}.delete_server', fake_delete_server)

        for ctid in servers_ctids:
            await server_service.server_repository.create_server(ctid)

        await server_service.delete_servers(servers_ctids)

        members = await redis.smembers(server_service.server_repository._servers_set_key)
        members = list(map(int, members))
        assert members == []

    @pytest.mark.asyncio
    async def test_delete_all_servers__failed(self):
        pass
