from aioredis import Redis
from fastapi import Depends

from vscale_ext.api.dependecies.redis import get_redis_pool
from vscale_ext.repositories.redis.servers import ServerRepository


async def get_server_repository(redis: Redis = Depends(get_redis_pool)):
    return ServerRepository(redis=redis)
