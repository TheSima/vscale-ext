from starlette.requests import Request

from vscale_ext.common.http_clients.vscale import VscaleHttpClient


def get_vscale_client(request: Request) -> VscaleHttpClient:
    return request.app.state.vscale_client
