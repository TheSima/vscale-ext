from aioredis import Redis
from starlette.requests import Request


def get_redis_pool(request: Request) -> Redis:
    return request.app.state.redis
