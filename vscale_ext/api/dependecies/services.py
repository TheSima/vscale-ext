from fastapi import Depends

from vscale_ext.api.dependecies.http_clients import get_vscale_client
from vscale_ext.api.dependecies.repositories import get_server_repository
from vscale_ext.common.http_clients.vscale import VscaleHttpClient
from vscale_ext.repositories.redis.servers import ServerRepository
from vscale_ext.services.servers.service import ServerService


def get_server_service(
        vscale_client: VscaleHttpClient = Depends(get_vscale_client),
        server_repository: ServerRepository = Depends(get_server_repository)):
    return ServerService(vscale_client=vscale_client, server_repository=server_repository)
