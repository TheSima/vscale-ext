from fastapi import APIRouter

from . import servers

router = APIRouter()


router.include_router(servers.router, prefix='/servers', tags=['Servers'])
