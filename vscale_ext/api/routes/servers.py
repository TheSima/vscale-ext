from fastapi import APIRouter, Query, Depends

from vscale_ext.api.dependecies.services import get_server_service
from vscale_ext.services.servers.service import ServerService

router = APIRouter()


@router.post('')
async def create_servers(
    quantity: int = Query(..., gt=0),
    server_service: ServerService = Depends(get_server_service),
):
    return await server_service.create_servers(quantity)


@router.delete('')
async def delete_all_servers(
    server_service: ServerService = Depends(get_server_service),
):
    return await server_service.delete_all_servers()
