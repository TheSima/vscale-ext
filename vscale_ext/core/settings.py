import os
from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    APP: str
    DEBUG: bool
    API_PREFIX: str

    REDIS_URL: str
    REDIS_DB: int
    REDIS_PASSWORD: Optional[str]

    REDIS_SERVERS_PREFIX: str

    VSCALE_API_HOST: str
    VSCALE_API_PREFIX: str
    VSCALE_COMMON_TIMEOUT: int  # seconds
    VSCALE_TOKEN: Optional[str]
    VSCALE_UID: Optional[str]
    VSCALE_SID: Optional[str]
    VSCALE_API_MAX_RETRIES: int

    class Config:
        """Config"""
        env_file = os.getenv('ENV', '.env')
        extra = 'forbid'


settings = Settings()
