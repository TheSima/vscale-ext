from abc import ABC, abstractmethod

from fastapi import HTTPException


class BaseHTTPException(HTTPException, ABC):
    """
    Base HTTP Exception for all http exceptions in app
    """

    @property
    @abstractmethod
    def default_status_code(self) -> int:
        ...

    @property
    @abstractmethod
    def default_detail(self) -> str:
        ...

    def __init__(self, status_code: int = None, detail: str = None):
        super().__init__(
            status_code=status_code or self.default_status_code,
            detail=detail or self.default_detail,
        )
