from fastapi import FastAPI

from vscale_ext.common.http_clients.vscale import VscaleHttpClient
from vscale_ext.repositories.redis.connection import get_redis_pool


def create_startup_handler(application: FastAPI):
    async def startup_handler() -> None:
        application.state.vscale_client = VscaleHttpClient()

        application.state.redis = await get_redis_pool()

    return startup_handler


def create_shutdown_handler(application: FastAPI):
    async def shutdown_handler() -> None:
        application.state.vscale_client.close_session()

        application.state.redis.close()
        await application.state.redis.wait_closed()

    return shutdown_handler
