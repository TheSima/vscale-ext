import aioredis
from aioredis import Redis

from vscale_ext.core.settings import settings


async def get_redis_pool() -> Redis:
    return await aioredis.create_redis_pool(
        settings.REDIS_URL,
        db=settings.REDIS_DB,
        password=settings.REDIS_PASSWORD,
    )
