from typing import List

from aioredis import Redis

from vscale_ext.core.settings import settings
from vscale_ext.repositories.errors import UniqueViolationError, NotFoundError


class ServerRepository:
    def __init__(self, redis: Redis):
        self._redis: Redis = redis
        self._servers_set_key: str = f'{settings.REDIS_SERVERS_PREFIX}_SET'

    async def create_server(self, ctid: int) -> None:
        is_exist = await self._redis.sismember(self._servers_set_key, ctid)
        if is_exist:
            raise UniqueViolationError(f'Server already created by ctid: {ctid}')
        print(f'Redis add {ctid}')
        self._redis.sadd(self._servers_set_key, ctid)

    async def delete_server(self, ctid: int) -> None:
        is_exist = await self._redis.sismember(self._servers_set_key, ctid)
        if not is_exist:
            raise NotFoundError(f"Server wasn't found by ctid: {ctid}")
        print(f'Redis remove {ctid}')
        self._redis.srem(self._servers_set_key, ctid)

    async def get_all_ctids(self) -> List[int]:
        set_members = await self._redis.smembers(self._servers_set_key)
        return [int(m) for m in set_members]
