class UniqueViolationError(Exception):
    """Raises when you violate unique constraint"""


class NotFoundError(Exception):
    """Raises when entity was not found"""
