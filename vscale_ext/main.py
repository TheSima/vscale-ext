from fastapi import FastAPI

from vscale_ext.api.routes.root import router as api_router
from vscale_ext.core.events import create_startup_handler, create_shutdown_handler
from vscale_ext.core.settings import settings


def get_application() -> FastAPI:
    application = FastAPI(
        title=settings.APP,
        debug=settings.DEBUG,
    )

    startup_handler = create_startup_handler(application)
    shutdown_handler = create_shutdown_handler(application)
    application.add_event_handler('startup', startup_handler)
    application.add_event_handler('shutdown', shutdown_handler)

    application.include_router(api_router, prefix=settings.API_PREFIX)
    return application


app = get_application()
