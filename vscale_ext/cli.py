import click
import uvicorn


@click.group()
def cli() -> None:
    """Initialize microservice"""


@cli.command()
@click.option('--host', default='127.0.0.1')
@click.option('--port', default=8080)
def serve(host: str, port: int) -> None:
    uvicorn.run('main:app', host=host, port=port, reload=True)


if __name__ == '__main__':
    cli()


